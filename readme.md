# Phonetic English

Transforms English to a phonetic spelling.

## Usage

Install the dependencies:

    npm install

Build the generated code file:

    npm run build

Run the tests.

    npm test

## License

ISC
